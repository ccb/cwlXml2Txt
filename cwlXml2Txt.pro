#-------------------------------------------------
#
# Project created by QtCreator 2014-11-14T14:04:19
#
#-------------------------------------------------

QT       += core gui xml
CONFIG += debug

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cwlXml2Txt
TEMPLATE = app


SOURCES += main.cpp\
        urltxtwidget.cpp

HEADERS  += urltxtwidget.h

DISTFILES = $$(QTDIR)/bin/QtCore.dll \
    $$(QTDIR)/bin/QtGui.dll \
    $$(QTDIR)/bin/QtXml.dll \
    $$(DESTDIR_TARGET)
