#ifndef URLTXTWIDGET_H
#define URLTXTWIDGET_H

#include <QWidget>
#include <Qlist>
#include <QStringList>
#include <QTextEdit>

class QXmlStreamReader;
class QDropEvent;
class QDragEnterEvent;
class QTextEdit;
class QPushButton;

class UrlTxtWidget : public QWidget
{
    Q_OBJECT

public:
    UrlTxtWidget(QWidget *parent = 0);
    ~UrlTxtWidget();

    bool OpenCwlXml(const QString& xmlFileName);
    bool Conver2Txt(const QString& xmlFileName, const QString& txtFileName);

protected:
    bool ParseCwlXml(const QString& xmlFileName, QStringList& list);
    void ParseSiteFiles(QXmlStreamReader& xml, QStringList& list);
    void ParseInputsUrl(QXmlStreamReader& xml, QStringList& list);
    void ParseVariation(QXmlStreamReader& xml, QStringList& list);

protected slots:
    void slotExport();
    void slotBatchMode(bool bDown);

private:
    void CreateUi();
    void UpdateText();

protected:
    virtual void dropEvent(QDropEvent * event);
    virtual void dragEnterEvent(QDragEnterEvent *event);
private:
    QStringList m_urlList;

    QTextEdit* m_pEdt;
    QPushButton* m_pBtnExport;
    QPushButton* m_pBtnBatch;
};

#endif // URLTXTWIDGET_H
